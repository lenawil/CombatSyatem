﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

/// <summary>
/// upon FixedUpdate call, dequeues logs into their appropriate textbox and adds a new line character for formatting purposes
/// </summary>
public class LogToTextbox : MonoBehaviour {

    public Text Textbox;
    
        

	// Use this for initialization
	void Start () {
	
	}
    void FixedUpdate()
    {
        LogToCombat();
        LogToEvent();
    }

    public void LogToCombat()
    {
        if (Messages.myCombatLog.Count > 0)
        {
            Textbox.text += Messages.myCombatLog.Dequeue();
            Textbox.text += '\n';
        }
    }

    public void LogToEvent()
    {
        if (Messages.myEventLog.Count > 0)
        {
            Textbox.text += Messages.myEventLog.Dequeue();
            Textbox.text += '\n';
        }
    }

}
