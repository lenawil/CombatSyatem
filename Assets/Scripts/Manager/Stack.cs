﻿using UnityEngine;
using System.Collections;

public class Stack : MonoBehaviour {

    /// <summary>
    /// handles the application and stacking of buffs
    /// </summary>
    /// <param name="Target"></param>
    /// <param name="NewBuff"></param>
    /// <param name="Source"></param>
    public static void Buff(GameObject Target, CreateNewBuff NewBuff, GameObject Source)
    {
        bool Unique = true;
        bool Dup = false;

        for (int i = 0; i < Target.GetComponent<Buffs>().BuffList.Count; i++)               // starts a for loop that iterates through all buffs currently attached to the taregt
        {
            if (Target.GetComponent<Buffs>().BuffList[i].BuffID == NewBuff.BuffID)
            {
                Dup = true;                                                                 // checking for duplicates so we know if this is a fresh application of if we need to stack the buff
                Debug.Log("Duplicate Detected");
                if (Target.GetComponent<Buffs>().BuffList[i].Stackable == true)             // if it is stackable, check and see if it is at max stacks yet
                {
                    int Stacks = Target.GetComponent<Buffs>().BuffList[i].CurrentStacks;
                    int MaxStacks = Target.GetComponent<Buffs>().BuffList[i].StackLimit;
                    Debug.Log(Stacks + " " + MaxStacks);

                    if (Stacks < MaxStacks)                                                     // if not, remove the old effect, increase the stacks by 1, re apply the effect
                    {
                        Debug.Log("Duplicate is Stackable");                                    // they get removed first so that increasing/decreasing buffs dont get spammed to keep them at max effect
                        Buffs.RemoveBuff(Target, NewBuff);
                        CreateNewBuff StackMe = Target.GetComponent<Buffs>().BuffList[i];
                        StackMe.Ticks = 0;
                        StackMe.CurrentStacks += 1;
                        StackMe.Applied = false;
                        Messages.CombatLog(LogTemplates.Stack(Target, StackMe.BuffName, Source, StackMe.CurrentStacks));
                    }

                    if (Stacks >= MaxStacks)                                                    // if it is, remove the old effect, reset duration, apply it again.
                    {
                        Debug.Log("Duplicate is at max stacks");
                        Buffs.RemoveBuff(Target, NewBuff);
                        CreateNewBuff StackMe = Target.GetComponent<Buffs>().BuffList[i];
                        StackMe.Ticks = 0;
                        StackMe.Applied = false;
                        Messages.CombatLog(LogTemplates.Stack(Target, StackMe.BuffName, Source, StackMe.CurrentStacks));
                    }

                }
                else
                {                                                                                   // if its not stackable, remove, destroy it, apply it again
                    Buffs.RemoveBuff(Target, NewBuff);
                    CreateNewBuff DeadMan = Target.GetComponent<Buffs>().BuffList[i];
                    Target.GetComponent<Buffs>().BuffList.Remove(Target.GetComponent<Buffs>().BuffList[i]);
                    CreateNewBuff.Destroy(DeadMan);
                    Target.GetComponent<Buffs>().BuffList.Add(NewBuff);
                    Messages.CombatLog(LogTemplates.GainsFrom(Target, NewBuff.BuffName, Source));
                }
            }
            else
            {
                Dup = false;
            }
        }

        if (Dup == false)                                                                 
        {
            int Count = Target.GetComponent<Buffs>().BuffList.Count;
            if (Count > 0)
            {
                for (int i = 0; i < Count; i++)
                {
                    if (Target.GetComponent<Buffs>().BuffList[i].BuffName == NewBuff.BuffName)
                    {
                        Messages.CombatLog(LogTemplates.BuffExists(Target.GetComponent<Buffs>().BuffList[i].BuffName, Target));
                        Unique = false;
                    }
                }
            }

            if (Unique == true)
            {
                Target.GetComponent<Buffs>().BuffList.Add(NewBuff);
            }
        }
    }

    /// <summary>
    /// handles the application and stacking of shields
    /// </summary>
    /// <param name="Target"></param>
    /// <param name="Shield"></param>
    /// <param name="Source"></param>
    public static void Shield(GameObject Target, CreateNewShield Shield, GameObject Source)
    {
        bool Dup = false;

        if (Target.GetComponent<Buffs>().ShieldList.Count > 0)  
        {

            for (int i = 0; i < Target.GetComponent<Buffs>().ShieldList.Count; i++)                             // if the target has any shield on it, start iterating through the list
            {
                if (Target.GetComponent<Buffs>().ShieldList[i].ShieldID == Shield.ShieldID)
                {
                    Dup = true;
                    if (Target.GetComponent<Buffs>().ShieldList[i].Stackable == true)                              // check for duplicates and if they are stackable
                    {
                        int Stacks = Target.GetComponent<Buffs>().ShieldList[i].CurrentStacks;
                        int MaxStacks = Target.GetComponent<Buffs>().ShieldList[i].StackLimit;

                        if (Stacks < MaxStacks)
                        {
                            CreateNewShield myShield = Target.GetComponent<Buffs>().ShieldList[i];                  // if its not at max stacks, add a stack and reset the duration/health
                            myShield.Ticks = 0;                                                                     // dont have to remve/reset these because we want people to keep the uptime high by recasting
                            myShield.CurrentStacks += 1;
                            myShield.CurrentHealth = myShield.MaxHealth * Stacks;
                            Messages.CombatLog(LogTemplates.Stack(Target, myShield.BuffName, Source, myShield.CurrentStacks));
                        }

                        if (Stacks >= MaxStacks)                                                                       // if its at max stacks, just refresh it by resetting the duration/health
                        {
                            CreateNewShield myShield = Target.GetComponent<Buffs>().ShieldList[i];
                            myShield.Ticks = 0;
                            myShield.CurrentHealth = myShield.MaxHealth * MaxStacks;
                            Messages.CombatLog(LogTemplates.Stack(Target, myShield.BuffName, Source, myShield.CurrentStacks));
                        }

                    }
                    else
                    {                                                                                               // if its a duplicate but not stackable, just reset it;
                        CreateNewShield myShield = Target.GetComponent<Buffs>().ShieldList[i];
                        myShield.Ticks = 0;
                        myShield.CurrentHealth = myShield.MaxHealth;
                        Messages.CombatLog(LogTemplates.GainsFrom(Target, myShield.BuffName, Source));
                    }
                }
                else
                {
                    Dup = false;                                                                                // if not a duplicate add a new one
                    Target.GetComponent<Buffs>().ShieldList.Add(Shield);
                    Messages.CombatLog(LogTemplates.GainsFrom(Target, Shield.BuffName, Source));
                }
            }
        }

        if (Dup == false)                                                                                       // adding a new one when the list has nothing in it already               
        {
            Target.GetComponent<Buffs>().ShieldList.Add(Shield);
            Messages.CombatLog(LogTemplates.GainsFrom(Target, Shield.BuffName, Source));
        }
    }

    /// <summary>
    /// handles the application and stacking of DOT's
    /// </summary>
    /// <param name="Target"></param>
    /// <param name="DOT"></param>
    /// <param name="Source"></param>
    public static void DOT(GameObject Target, CreateNewDOT DOT, GameObject Source)
    {
        bool Dup = false;

        if (Target.GetComponent<Buffs>().DOTList.Count > 0)
        {

            for (int i = 0; i < Target.GetComponent<Buffs>().DOTList.Count; i++)                             // if the target has any shield on it, start iterating through the list
            {
                if (Target.GetComponent<Buffs>().DOTList[i].DOT_ID == DOT.DOT_ID)
                {
                    Messages.Message("DOT is a duplicate");
                    Dup = true;
                    if (Target.GetComponent<Buffs>().DOTList[i].Stackable == true)                              // check for duplicates and if they are stackable
                    {
                        
                        CreateNewDOT myDOT = Target.GetComponent<Buffs>().DOTList[i];
                        int Stacks = myDOT.CurrentStacks;
                        int MaxStacks = myDOT.StackLimit;

                        if (Stacks < MaxStacks)
                        {                                                                                         // if its not at max stacks, add a stack and reset the duration/health
                            myDOT.Ticks = 0;                                                                     // dont have to remve/reset these because we want people to keep the uptime high by recasting
                            myDOT.CurrentStacks += 1;
                            myDOT.CurrentDamage = myDOT.Damage * myDOT.CurrentStacks;
                            Messages.Message("DOT is stackable: currently at " + myDOT.CurrentStacks);
                            Messages.CombatLog(LogTemplates.Stack(Target, myDOT.DOTName, Source, myDOT.CurrentStacks));
                        }

                        if (Stacks >= MaxStacks)                                                                       // if its at max stacks, just refresh it by resetting the duration/health
                        {
                            myDOT.Ticks = 0;
                            myDOT.CurrentDamage = myDOT.Damage * MaxStacks;
                            Messages.Message("DOT is at max stacks");
                        }

                    }
                    else
                    {                                                                                               // if its a duplicate but not stackable, just reset it;
                        CreateNewDOT myDOT = Target.GetComponent<Buffs>().DOTList[i];
                        myDOT.Ticks = 0;
                        Messages.CombatLog(LogTemplates.GainsFrom(Target, DOT.DOTName, Source));
                    }
                }
                else
                {
                    Dup = false;                                                                                // if not a duplicate add a new one
                    Target.GetComponent<Buffs>().DOTList.Add(DOT);
                    Messages.CombatLog(LogTemplates.GainsFrom(Target, DOT.DOTName, Source));
                }
            }
        }

        if (Dup == false)                                                                                       // adding a new one when the list has nothing in it already               
        {
            Target.GetComponent<Buffs>().DOTList.Add(DOT);
            Messages.CombatLog(LogTemplates.GainsFrom(Target, DOT.DOTName, Source));
        }
    }
}
